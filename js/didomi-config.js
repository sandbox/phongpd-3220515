/**
 * @file
 * Attaches several event listener to a web page (debugging version).
 */

(function (drupalSettings) {
  window.didomiConfig = {
    website: {
      apiKey: drupalSettings.didomi_setting.apiKey,
      name: drupalSettings.didomi_setting.siteName,
      logoUrl: drupalSettings.didomi_setting.logoPath,
      privacyPolicyURL: drupalSettings.didomi_setting.policyUrl,
      vendors: {
        didomi: drupalSettings.didomi_setting.didomiVendors,
        iab: drupalSettings.didomi_setting.iabVendors,
        custom: drupalSettings.didomi_setting.customVendors
      },
    },
    languages: {
      enabled: ['fr'],
    },
    tagManager: {
      provider: 'gtm'
    },
    theme: {
      color: '#002a45'
    },
    notice: {
      position: 'bottom',
      closeOnClick: drupalSettings.didomi_setting.notice.close_on_click,
      content: {
        notice: {
          fr: drupalSettings.didomi_setting.notice.content.notice_text
        },
        learnMore: {
          fr: drupalSettings.didomi_setting.notice.content.learn_more
        },
        dismiss: {
          fr: drupalSettings.didomi_setting.notice.content.dismiss
        }
      },
    },
    preferences: {
      content: {
        text: {
          fr: drupalSettings.didomi_setting.preferences.content.text
        },
        title: {
          fr: drupalSettings.didomi_setting.preferences.content.title
        },
        textVendors: {
          fr: drupalSettings.didomi_setting.preferences.content.text_vendors
        },
        titleVendors: {
          fr: drupalSettings.didomi_setting.preferences.content.title_vendors
        }
      }
    }
  };
})( drupalSettings);
