<?php

namespace Drupal\didomi_cookie_compliance\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class CookieManagerForm.
 */
class CookieManagerForm extends ConfigFormBase {

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The file system.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Constructs a new CookieManagerForm.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system.
   */
  public function __construct(ConfigFactoryInterface $config_factory, MessengerInterface $messenger, FileSystemInterface $file_system) {
    parent::__construct($config_factory);
    $this->messenger = $messenger;
    $this->fileSystem = $file_system;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('messenger'),
      $container->get('file_system')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'didomi_cookie_compliance.config',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'didomi_cookie_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('didomi_cookie_compliance.config');

    $form['general'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('General'),
    ];
    $form['general']['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Key'),
      '#maxlength' => 100,
      '#size' => 100,
      '#default_value' => $config->get('api_key'),
    ];
    $form['general']['site_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Site name'),
      '#maxlength' => 100,
      '#size' => 100,
      '#default_value' => $config->get('site_name'),
    ];
    $form['general']['logo_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Logo path'),
      '#maxlength' => 256,
      '#size' => 300,
      '#default_value' => $config->get('logo_path'),
    ];
    $form['general']['policy_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Policy Url'),
      '#maxlength' => 256,
      '#size' => 300,
      '#default_value' => $config->get('policy_url'),
    ];

    $form['notice'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Notice'),
    ];
    $form['notice']['content']['notice_text'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Notice '),
      '#default_value' => $config->get('notice.content.notice_text'),
    ];
    $form['notice']['content']['learn_more'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Learn More'),
      '#description' => $this->t('Button \'Learn More\''),
      '#default_value' => $config->get('notice.content.learn_more'),
    ];
    $form['notice']['content']['dismiss'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Agree'),
      '#description' => $this->t('Button \'Agree\''),
      '#default_value' => $config->get('notice.content.dismiss'),
    ];
    $form['notice']['close_on_click'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Close On Click'),
      '#default_value' => $config->get('notice.close_on_click'),
    ];

    $form['preferences'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Preferences'),
    ];
    $form['preferences']['content']['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#description' => $this->t('Title of the popup (Welcome to ...)'),
      '#default_value' => $config->get('preferences.content.title'),
    ];
    $form['preferences']['content']['text'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Text'),
      '#description' => $this->t('Main content of the popup'),
      '#default_value' => $config->get('preferences.content.text'),
    ];
    $form['preferences']['content']['title_vendors'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Vendors title'),
      '#description' => $this->t('Title of the vendors popup (Select partners for ...)'),
      '#default_value' => $config->get('preferences.content.title_vendors'),
    ];
    $form['preferences']['content']['text_vendors'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Vendors text'),
      '#description' => $this->t('Main content of the vendors popup'),
      '#default_value' => $config->get('preferences.content.text_vendors'),
    ];

    $form['vendors'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Vendors'),
    ];
    $form['vendors']['didomi_vendors'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Didomi Vendors'),
      '#maxlength' => 200,
      '#description' => 'Comma-separated',
      '#size' => 200,
      '#default_value' => $config->get('didomi_vendors'),
    ];
    $form['vendors']['iab_vendors'] = [
      '#type' => 'textfield',
      '#title' => $this->t('IAB Vendors'),
      '#maxlength' => 200,
      '#description' => 'Comma-separated',
      '#size' => 200,
      '#default_value' => $config->get('iab_vendors'),
    ];
    $form['vendors']['custom_vendors'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Custom Vendors'),
      '#description' => 'Comma-separated',
      '#maxlength' => 200,
      '#size' => 200,
      '#default_value' => $config->get('custom_vendors'),
    ];
    $form['web_property_id_google'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Web Property ID'),
      '#placeholder' => "UA-",
      '#maxlength' => 200,
      '#size' => 200,
      '#default_value' => $config->get('web_property_id_google'),
    ];
    $form['didomi_cookie_compliance_enable_cookie'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable cookie'),
      '#default_value' => $config->get('didomi_cookie_compliance_enable_cookie'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('didomi_cookie_compliance.config')
      ->set('api_key', $form_state->getValue('api_key'))
      ->set('site_name', $form_state->getValue('site_name'))
      ->set('logo_path', $form_state->getValue('logo_path'))
      ->set('policy_url', $form_state->getValue('policy_url'))
      ->set('notice', [
        'close_on_click' => $form_state->getValue('close_on_click'),
        'content' => [
          'notice_text' => $form_state->getValue('notice_text'),
          'dismiss' => $form_state->getValue('dismiss'),
          'learn_more' => $form_state->getValue('learn_more'),
        ],
      ])
      ->set('preferences', [
        'content' => [
          'title'=> $form_state->getValue('title'),
          'text' => $form_state->getValue('text'),
          'title_vendors' => $form_state->getValue('title_vendors'),
          'text_vendors' => $form_state->getValue('text_vendors'),
        ],
      ])
      ->set('didomi_vendors', $form_state->getValue('didomi_vendors'))
      ->set('iab_vendors', $form_state->getValue('iab_vendors'))
      ->set('custom_vendors', $form_state->getValue('custom_vendors'))
      ->set('web_property_id_google', $form_state->getValue('web_property_id_google'))
      ->set('didomi_cookie_compliance_enable_cookie', $form_state->getValue('didomi_cookie_compliance_enable_cookie'))
      ->save();

    $this->createAssets();
  }

  /**
   * Prepares directory for and saves snippet files based on current settings.
   *
   * @return bool
   *   Whether the files were saved.
   */
  public function createAssets() {
    $result = TRUE;
    $directory = 'public://didomi_tag';
    if (!is_dir($directory) || !_didomi_cookie_compliance_is_writable($directory) || !_didomi_cookie_compliance_is_executable($directory)) {
      $result = $this->fileSystem->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS);
    }
    if ($result) {
      $result = $this->saveSnippets();
    }
    else {
      $args = ['%directory' => $directory];
      $message = 'The directory %directory could not be prepared for use, possibly due to file system permissions. The directory either does not exist, or is not writable or searchable.';
      $this->displayMessage($message, $args, 'error');
      \Drupal::logger('google_tag')->error($message, $args);
    }
    return $result;
  }

  /**
   * Saves JS snippet file based on current settings.
   *
   * @return bool
   *   Whether the files were saved.
   */
  public function saveSnippets() {
    // Save the altered snippets after hook_google_tag_snippets_alter().
    module_load_include('inc', 'didomi_cookie_compliance', 'includes/snippet');
    $result = TRUE;
    $snippet = _didomi_cookie_compliance_script_snippet();
    $path = $this->fileSystem->saveData((string) $snippet, "public://didomi_tag/didomi_tag.script.js", FileSystemInterface::EXISTS_REPLACE);
    $result = !$path ? FALSE : $result;
    if (!$result) {
      $message = 'An error occurred saving snippet file. Contact the site administrator if this persists.';
      $this->displayMessage($message, [], 'error');
      \Drupal::logger('didomi_tag')->error($message, []);
    }
    else {
      $message = 'Created snippet files based on configuration.';
      $this->displayMessage($message, []);
      \Drupal::service('asset.js.collection_optimizer')->deleteAll();
      _drupal_flush_css_js();
    }
    return $result;
  }

  /**
   * Displays a message to admin users.
   *
   * @see arguments to t() and drupal_set_message()
   */
  public function displayMessage($message, $args = [], $type = 'status') {
    global $google_tag_display_message;
    if ($google_tag_display_message) {
      $this->messenger->addMessage($this->t($message, $args), $type);
    }
  }

}
